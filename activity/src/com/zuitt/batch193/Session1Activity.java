package com.zuitt.batch193;

import java.util.Scanner;

public class Session1Activity {
    public static void main(String[] args) {
        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();
        System.out.println("Last Name:");
        String lastName = appScanner.nextLine().trim();
        System.out.println("First Subject Grade:");
        double firstGrade = appScanner.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondGrade = appScanner.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdGrade = appScanner.nextDouble();
        int gradeAvg = (int) ((firstGrade + secondGrade + thirdGrade) / 3);

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + gradeAvg);
    }
}
