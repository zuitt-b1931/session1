package com.zuitt.batch193;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World");
    }

    // public = Access Modifiers - Who can see this method
    // static = Non-access Modifier - How should this method behave
        // static keyword field exists across all the class instances
    // void = Return Type - What should this method return
        // void keyword specifies that a method doesn't return anything
    // main = Method name -

}
